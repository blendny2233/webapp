'use strict';

var stepConstants = {
  MAIN: 'main',
  GITHUB_PROJECTS: 'github_projects',
  OVERVIEW: 'overview'
};

module.exports = stepConstants;
